This is a game potatoxel(i.e me) and sofviic made in 118minutes (The initial commit(excluding writing the readme and license information) is made in 118minutes, any subsequent commits are not bound).

The game's code is licensed under the AGPLv3.0 only and is coded by potatoxel (me), and the assets (art + sounds) are made by sofviic and are licensed under the CC-BY-SA-4.0


This is a game called Brian's Brian.
Code Copyright (C) 2022 waleed177 <potatoxel.org>  
Art and Sound Copyright (C) 2022 sofviic <sofviic.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License only.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

