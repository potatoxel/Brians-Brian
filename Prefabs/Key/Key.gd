extends Area2D

var _player: Player

func _on_Key_body_entered(body):
	if body is Player:
		_player = body

func _on_Key_body_exited(body):
	if body is Player:
		_player = null

func _process(delta):
	if Input.is_action_just_pressed("pickup_key"):
		if _player:
			$KeyPickup.play()
			set_process(false)
			_player.stunned = true
			yield(get_tree().create_timer(1), "timeout")
			_player.stunned = false
			_player.keys += 1
			queue_free()
