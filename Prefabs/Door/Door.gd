extends Area2D

var _player

func _on_Door_body_entered(body):
	if body is Player:
		_player = body

func _on_Door_body_exited(body):
	if body is Player:
		_player = null

func _input(event):
	if Input.is_action_just_pressed("open_door"):
		if _player:
			_player.stunned = true
			if _player.keys >= 2:
				$DoorOpened.play()
				yield(get_tree().create_timer(2.5), "timeout")
				get_tree().change_scene("res://Scenes/Win/Win.tscn")
			else:
				$LockedDoor.play()
				yield(get_tree().create_timer(2.5), "timeout")
			_player.stunned = false
