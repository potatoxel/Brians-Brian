extends Area2D

var speed = 0

func _process(delta):
	if Globals.player.keys == 0:
		speed = 0.2 * 500
	elif Globals.player.keys == 1:
		speed = 0.3 * 500
	elif Globals.player.keys == 2:
		speed = 0.5 * 500
		
	var dir = (Globals.player.position - position).normalized()
	position += dir * speed * delta
	if abs(dir.x) > abs(dir.y):
		if dir.x > 0:
			$Sprite.frame_coords = Vector2(2, 1)
		else:
			$Sprite.frame_coords = Vector2(0, 1)
	else:
		if dir.y > 0:
			$Sprite.frame_coords = Vector2(1, 1)
		else:
			$Sprite.frame_coords = Vector2(3, 1)


func _on_BriansBrian_body_entered(body):
	if body is Player:
		get_tree().change_scene("res://Scenes/GameOver/GameOver.tscn")
