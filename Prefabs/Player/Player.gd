class_name Player
extends KinematicBody2D

var speed = 500
var keys = 0 setget _set_keys
var stunned = false

func _set_keys(value):
	keys = value
	$"../CanvasLayer/Keys".texture.region = Rect2(value * 64, 0, 64, 64)

func _process(delta):
	var velocity = Vector2.ZERO
	if Input.is_action_pressed("ui_left"): 
		velocity += Vector2.LEFT * speed 
		$Player.frame = 0
	if Input.is_action_pressed("ui_right"): 
		velocity += Vector2.RIGHT * speed
		$Player.frame = 2
	if Input.is_action_pressed("ui_up"): 
		velocity += Vector2.UP * speed 
		$Player.frame = 3
	if Input.is_action_pressed("ui_down"): 
		velocity += Vector2.DOWN * speed 
		$Player.frame = 1
	if stunned:
		velocity = Vector2.ZERO
	move_and_slide(velocity.normalized()*speed)
