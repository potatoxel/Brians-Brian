extends Node2D

func _ready():
	Globals.player = $Player

func _process(delta):
	if Input.is_action_just_pressed("map"):
		$CanvasLayer/MapBig.visible = not $CanvasLayer/MapBig.visible
		Globals.player.stunned = $CanvasLayer/MapBig.visible
		if Globals.player.stunned:
			$OpenMapSound.play()
		else:
			$CloseMapSound.play()
